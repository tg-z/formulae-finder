import "./styles.css";

const URL = "https://formulae.brew.sh/api/formula.json";
let formulaList = [];
const $search = document.getElementById("search");
const $results = document.getElementById("results");
const $stats = document.getElementById("stats");

const getFormulaList = async () => {
  const response = await fetch(URL);
  formulaList = await response.json();
  updateFormulaList(formulaList);
};

const updateFormulaList = formulaList => {
  const output = formulaList
    .map(
      formula =>
        `<li class="formula block rounded p-2 mb-3 bg-white hover:bg-blue-100 leading-loose">
          <a class="text-lg font-normal text-blue-800 hover:underline" target="_blank" href="${formula.homepage}">${formula.name}</a>
          <div class="block">
	  <p class="text-base text-gray-600 leading-normal">${formula.desc}</p>
            <code class="inline font-hairline text-sm">$ </code>
            <code class="inline font-hairline text-sm">brew install ${formula.name}<code>
          </div>
        </li>`
    )
    .join("");
  $stats.innerHTML = `Listing ${formulaList.length} packages.`;
  $results.innerHTML = output;
};

const runSearch = e => {
  const output = formulaList.filter(formula => formula.name.indexOf(e) === 0);
  updateFormulaList(output);
};

$search.addEventListener("keyup", e => runSearch(e.target.value));

getFormulaList();
